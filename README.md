# Roblox Arch Linux Installer
A small bash script designed to make the first install of Roblox easier.

### Note! I am not affiliated with Roblox - just looked at making an installer.

This should work on Arch Linux and Manjaro without any issues.

Artix users: enable the Arch repos if they're necessary for you. I don't have access to an Artix install right now.

### For those using alternatives to sudo on systems without sudo:
Edit the main script to use your choice. Support for doas right now is planned for a later update, with custom parameters coming a bit later than that.

### Doesn't work?
Start an issue here or DM me on Discord (meme.#3299).
