# Roblox Arch Linux Installer for Arch-based distributions

# Prerequisite checks.
gitC="$(sudo pacman -Qs git > /dev/null)"

if [ -n "$gitC" ]
then
	echo "Git is required to install the AUR packages used in this script. Install now? (y/n)"
	read gitP
	if [ "$gitP" == "y" ]
	then
		sudo pacman -S git
	fi
fi

bdC="$(sudo pacman -Qs base-devel > /dev/null)"

if [ -n "$bdC" ]
then
	echo "The base-devel package is required to install the AUR packages used in this script. Install now? (y/n)"
	read bdP
	if [ "$bdP" == "y" ]
	then
		sudo pacman -S base-devel
	fi
fi

# Installing.
	
gjC="$(sudo pacman -Qi grapejuice-git > /dev/null)"

if [ ! -n "$gjC" ]
then
	echo "Downloading grapejuice from the AUR..."
	mkdir ~/RALITEMP
	cd ~/RALITEMP
	git clone https://aur.archlinux.org/grapejuice-git.git
	cd grapejuice-git
	makepkg -si
	echo "Now that Grapejuice is installed, would you like to install the patched Wine build before installing Roblox?"
	echo "This fixes MANY mouse bugs, but will NOT fix Studio (it is heavily broken, more broken than pre-DXVK D3D11) (y/n)"
	read wtP
	if [ "wtP" == "y" ]
	then
		wget https://cdn.discordapp.com/attachments/858117357897121822/859555850339483688/wine-tkg-staging-fsync-git-6.11.r0.g432c0b5a-326-x86_64.pkg.tar.zst
		sudo pacman -U wine-tkg-staging-fsync-git-6.11.r0.g432c0b5a-326-x86_64.pkg.tar.zst
	fi
	grapejuice install-roblox
	echo "Now that you have a wineprefix, you can install DXVK. Installing DXVK can increase your performance drastically."
	echo "Note that this WILL NOT work on systems without a Vulkan-capable graphics card or driver. (y/n)"
	read dxvkP
	if [ "$dxvkP" == "y" ]
	then
		cd ~/RALITEMP
		wget https://github.com/doitsujin/dxvk/releases/download/v1.9/dxvk-1.9.tar.gz
		tar -xzf dxvk-1.9.tar.gz
		cd dxvk-1.9
		export WINEPREFIX=~/.local/share/grapejuice/wineprefix
		./setup_dxvk.sh install --with-d3d10
	fi
	rm -rf ~/RALITEMP
fi

echo "Everything is done. You can now play Roblox by heading to the website and starting a game. Some browsers have extra steps, so follow those accordingly."
